<?php

namespace App\Exception;


class MultiException extends \Exception
{
    protected $data = [];

    public function add(\Exception $e)
    {
        $this->data[] = $e;
    }

    public function getErrors()
    {
        return $this->data;
    }

}